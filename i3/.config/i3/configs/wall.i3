# color scheme for i3 based on wall


set_from_resource $bg-color i3wm.color0
set_from_resource $text-color i3wm.color15
set_from_resource $urgent-bg-color i3wm.color2
set_from_resource $inactive-text-color i3wm.color7
set_from_resource $accent i3wm.color4
set $wallpaper /home/joaj/Pictures/walls/gruvbox_astronaut.jpg

# vim:filetype=i3
