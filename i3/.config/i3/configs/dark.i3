# Dark color scheme for i3

# set wallpapper
set $wallpapper "~/Pictures/archlinux.png"
exec_always --no-startup-id feh --bg-scale $wallpapper

set $bg-color            #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
set $border              #000000
set $arc-blue            #4084D6
#set $arc-blue            #9D2D28
# vim:filetype=i3

