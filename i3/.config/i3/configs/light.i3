# Light color scheme for i3

# set wallpapper
set $wallpapper "/home/joaj/media/arch.png"
exec_always --no-startup-id feh --bg-scale $wallpapper

set $bg-color            #f8f5e2
set $text-color          #676e7d
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
set $border              #000000
set $arc-blue            #4084D6

# vim:filetype=i3
