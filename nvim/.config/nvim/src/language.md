# Languages


Setup python hosts.
```viml
let g:python3_host_prog='/usr/bin/python3'
let g:python_host_prog='/usr/bin/python2'
```

```viml
"autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab smarttab
"autocmd FileType vue setlocal ts=2 sts=2 sw=2 expandtab smarttab
"autocmd FileType js setlocal ts=2 sts=2 sw=2 expandtab smarttab

" vim: set sw=2 ts=2 et foldlevel=0 foldmethod=marker:
```

<!-- vim: set ft=vim: set conceallevel=0 -->
