# Limelight

[Plugins](plugins.md)

[https://github.com/junegunn/limelight.vim](https://github.com/junegunn/limelight.vim)

## Configuration

```viml
let g:limelight_default_coefficient = 0.7
let g:limelight_conceal_ctermfg = 238
nmap <silent> gl :Limelight!!<CR>
xmap gl <Plug>(Limelight)
```
