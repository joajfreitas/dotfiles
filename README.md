# README

## Vim configuration

[https://joajfreitas.gitlab.io/dotfiles/vimrc.html](https://joajfreitas.gitlab.io/dotfiles/vimrc.html)

## GPG key

### Export

    gpg --armor --export-private-keys <email> > private.key
	gpg --symmetric private.key

### Account recovery

Recover gpg key

	gpg -d key.gpg

Import gpg private key

	gpg --import pgp-private-keys.asc

Clone the password-store.

	git clone https://gitlab.com/joajfreitas/pass
